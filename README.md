### SimpleChat as a messager, allowing text messaging to peaple within the network 

### [Demo on Heroku](https://wbsimplechat.herokuapp.com/)

### Uses
- Java 11
- SpringBoot 2
- H2DB
- Thymeleaf
- Bootstrap
- Jquery and Noty
- StompJs

### Help
1. Login in
2. Create chat, open
3. Bind companion to chat
3. When he comes in you can talk to him