/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.socket;

import com.gitlab.witalijbukatkin.simplechats.model.Message;
import com.gitlab.witalijbukatkin.simplechats.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class MessageWebSocketController {
    private final SimpMessagingTemplate template;
    private final MessageService service;

    @Autowired
    public MessageWebSocketController(SimpMessagingTemplate template, MessageService service) {
        this.template = template;
        this.service = service;
    }

    @MessageMapping("/{chatId}")
    public void sendMessage(@DestinationVariable Long chatId, @Payload Message message) {
        service.create(message.getData(), chatId, message.getSenderId());
        template.convertAndSend("/topic/" + chatId, message);
    }
}
