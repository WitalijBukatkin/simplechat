/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.service;

import com.gitlab.witalijbukatkin.simplechats.exception.NotFoundException;
import com.gitlab.witalijbukatkin.simplechats.model.Chat;
import com.gitlab.witalijbukatkin.simplechats.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

import static com.gitlab.witalijbukatkin.simplechats.util.UserUtil.normalizeUser;

@Service
public class ChatService {
    private final ChatRepository repository;

    @Autowired
    public ChatService(@Qualifier("dataJpaChatRepositoryImpl") ChatRepository repository) {
        this.repository = repository;
    }

    public Chat create(String userName, String name) {
        String normalize = normalizeUser(userName);

        Assert.notNull(normalize, "userName must not be null");
        Assert.notNull(name, "name must not be null");

        Chat created = repository.save(
                new Chat(normalize, name, List.of(normalize)), normalize);

        if (created == null) {
            throw new IllegalArgumentException();
        }

        return created;
    }

    public void delete(long id, String userName) {
        String normalize = normalizeUser(userName);

        Assert.notNull(normalize, "userName must not be null");
        if (!repository.delete(id, normalize)) {
            throw new NotFoundException();
        }
    }

    public Chat get(long id, String userName) {
        String normalize = normalizeUser(userName);

        Assert.notNull(normalize, "userName must not be null");
        Chat chat = repository.get(id, normalize);

        if (chat == null) {
            throw new NotFoundException();
        }

        return chat;
    }

    public List<Chat> getAll(String userName) {
        String normalize = normalizeUser(userName);
        Assert.notNull(normalize, "userName must not be null");
        return repository.getAll(normalize);
    }

    public void bindUser(long id, String userName, String newUserName) {
        String normalizeUser = normalizeUser(userName);
        String normalizeNewUser = normalizeUser(newUserName);

        Assert.notNull(normalizeUser, "userName must not be null");
        Assert.notNull(normalizeNewUser, "newUserName must not be null");
        Chat chat = get(id, normalizeUser);

        chat.getUsers().add(normalizeNewUser);

        if (repository.save(chat, normalizeUser) == null) {
            throw new IllegalStateException("Not save");
        }
    }

    public void unbindUser(long id, String userName, String unbindUserName) {
        String normalizeUser = normalizeUser(userName);
        String normalizeUnbindUser = normalizeUser(unbindUserName);

        Assert.notNull(normalizeUser, "userName must not be null");
        Assert.notNull(normalizeUnbindUser, "unbindUserName must not be null");
        Chat chat = get(id, normalizeUser);

        if (chat.getAdmin().equals(normalizeUnbindUser)) {
            throw new IllegalStateException("Admin isn't unbind");
        }

        if (!chat.getUsers().remove(normalizeUnbindUser)) {
            throw new NotFoundException();
        }

        if (repository.save(chat, normalizeUser) == null) {
            throw new IllegalStateException("Not save");
        }
    }

    public void rename(long id, String userName, String name) {
        Assert.notNull(userName, "userName must not be null");
        Assert.isTrue(!name.replace(" ", "").isEmpty(), "name must not be null");
        Chat chat = get(id, userName);

        if (!chat.getAdmin().equals(userName)) {
            throw new IllegalStateException("You isn't chat admin");
        }

        chat.setName(name);
        repository.save(chat, userName);
    }
}
