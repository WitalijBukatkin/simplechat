/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.service;

import com.gitlab.witalijbukatkin.simplechats.exception.NotFoundException;
import com.gitlab.witalijbukatkin.simplechats.model.Message;
import com.gitlab.witalijbukatkin.simplechats.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

import static com.gitlab.witalijbukatkin.simplechats.util.UserUtil.normalizeUser;

@Service
public class MessageService {

    private final MessageRepository repository;

    @Autowired
    public MessageService(@Qualifier("dataJpaMessageRepositoryImpl") MessageRepository repository) {
        this.repository = repository;
    }

    public Message create(String text, long chatId, String userName) {
        String normalize = normalizeUser(userName);

        Assert.notNull(text, "message must not be null");
        Assert.isTrue(!text.replace(" ", "").isEmpty(), "message must not be empty");
        Assert.notNull(normalize, "userName must not be null");

        Assert.doesNotContain("<", text, "data contains <");

        Message created = repository.save(
                new Message(normalize, text), chatId, normalize);

        if (created == null) {
            throw new IllegalArgumentException("Message isn't created!");
        }

        return created;
    }

    public List<Message> getAllOfChat(long chatId, String userName) {
        String normalize = normalizeUser(userName);

        Assert.notNull(normalize, "userName must not be null");

        List<Message> messages = repository.getAllOfChat(chatId, normalize);

        if (messages == null) {
            throw new NotFoundException();
        }

        return messages;
    }
}
