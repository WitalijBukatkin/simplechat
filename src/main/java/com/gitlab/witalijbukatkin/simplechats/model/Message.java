/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Entity
public class Message extends BaseEntity {
    @JsonProperty(access = WRITE_ONLY)
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Chat chat;

    @NotEmpty
    private String senderId;

    @NotEmpty
    private String data;

    public Message(@NotNull Long id, Chat chat, @NotEmpty String senderId, @NotEmpty String data) {
        super(id);

        this.chat = chat;
        this.senderId = senderId;
        this.data = data;
    }

    public Message(@NotEmpty String senderId, @NotEmpty String data) {
        this(null, null, senderId, data);
    }

    public Message() {
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(chat, message.chat) &&
                Objects.equals(senderId, message.senderId) &&
                Objects.equals(data, message.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chat, senderId, data);
    }

    @Override
    public String toString() {
        return "Message{" +
                "chat=" + chat +
                ", senderId='" + senderId + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
