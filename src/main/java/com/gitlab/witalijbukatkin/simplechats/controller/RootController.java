/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RootController {

    @GetMapping("/")
    public String login(@RequestParam(required = false) String userName) {
        if (userName == null) {
            return "login";
        } else {
            return "redirect:/messages?userName=" + userName;
        }
    }

    @GetMapping("/messages")
    public String messages(@RequestParam String userName, Model model) {
        model.addAttribute("userName", userName);
        return "messages";
    }

    @GetMapping("/logout")
    public String logout() {
        return "redirect:";
    }
}
