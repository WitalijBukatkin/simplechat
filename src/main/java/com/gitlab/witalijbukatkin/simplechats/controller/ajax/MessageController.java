/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.controller.ajax;

import com.gitlab.witalijbukatkin.simplechats.model.Message;
import com.gitlab.witalijbukatkin.simplechats.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = MessageController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class MessageController {
    public static final String REST_URL = "/ajax/chats/{chatId}/messages";
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final MessageService service;

    @Autowired
    public MessageController(MessageService service) {
        this.service = service;
    }

    @GetMapping
    public List<Message> getAll(@RequestParam String userName, @PathVariable long chatId) {
        log.info("getAll for userName {}", userName);
        return service.getAllOfChat(chatId, userName);
    }
}