/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.controller.ajax;

import com.gitlab.witalijbukatkin.simplechats.model.Chat;
import com.gitlab.witalijbukatkin.simplechats.service.ChatService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = ChatController.REST_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class ChatController {
    public static final String REST_URL = "/ajax/chats";
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ChatService service;

    @Autowired
    public ChatController(ChatService service) {
        this.service = service;
    }

    @GetMapping
    public List<Chat> getAll(@RequestParam String userName) {
        log.info("getAll for userName {}", userName);
        return service.getAll(userName);
    }

    @GetMapping("/{id}")
    public Chat get(@RequestParam String userName, @PathVariable long id) {
        log.info("{} get with userName {}", id, userName);
        return service.get(id, userName);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<Chat> create(@RequestParam String userName, @RequestParam String name) {
        log.info("create {} with userName {}", name, userName);

        Chat created = service.create(userName, name);

        URI uriOfNewResource = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(REST_URL + "/" + created.getId())
                .build().toUri();

        return ResponseEntity.created(uriOfNewResource).body(created);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam String userName, @PathVariable long id) {
        log.info("{} delete with userName {}", id, userName);
        service.delete(id, userName);
    }

    @PutMapping("/{id}/bindUser")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void bindUser(@RequestParam String userName, @PathVariable long id, @RequestParam String newUserName) {
        log.info("{} bindUser newUserName {} with userName {}", id, newUserName, userName);
        service.bindUser(id, userName, newUserName);
    }

    @PutMapping("/{id}/unbindUser")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void unbindUser(@RequestParam String userName, @PathVariable long id, @RequestParam String unbindUserName) {
        log.info("{} unbindUser {} with userName {}", id, unbindUserName, userName);
        service.unbindUser(id, userName, unbindUserName);
    }

    @PutMapping("/{id}/rename")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void rename(@RequestParam String userName, @PathVariable long id, @RequestParam String name) {
        log.info("{} rename to {} with userName {}", id, name, userName);
        service.rename(id, userName, name);
    }
}
