/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.util;

public class UserUtil {
    public static String normalizeUser(String userName) {
        if (userName == null) {
            return null;
        }

        String s = userName.replace(" ", "").toLowerCase();

        if (s.isEmpty()) {
            return null;
        }

        return s;
    }
}
