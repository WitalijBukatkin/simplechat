/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.repository.datajpa;

import com.gitlab.witalijbukatkin.simplechats.model.Message;
import com.gitlab.witalijbukatkin.simplechats.repository.MessageRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DataJpaMessageRepositoryImpl implements MessageRepository {
    private final CrudMessageRepository messageRepository;
    private final DataJpaChatRepositoryImpl chatRepository;

    public DataJpaMessageRepositoryImpl(CrudMessageRepository messageRepository, DataJpaChatRepositoryImpl chatRepository) {
        this.messageRepository = messageRepository;
        this.chatRepository = chatRepository;
    }

    @Override
    public Message save(Message message, long chatId, String userName) {
        if (!message.isNew() && messageRepository.findById(message.getId()).isPresent() ||
                !chatRepository.isExistUserInChat(chatId, userName)) {
            return null;
        }
        message.setChat(chatRepository.get(chatId, userName));
        return messageRepository.save(message);
    }

    @Override
    public List<Message> getAllOfChat(long chatId, String userName) {
        if (!chatRepository.isExistUserInChat(chatId, userName)) {
            return null;
        }

        return messageRepository.findAllOfChat(chatId);
    }
}
