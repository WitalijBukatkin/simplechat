/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.repository.datajpa;

import com.gitlab.witalijbukatkin.simplechats.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface CrudMessageRepository extends JpaRepository<Message, Long> {

    @Override
    @Transactional
    void delete(Message message);

    @Override
    @Transactional
    Message save(Message message);

    @Query("from Message where chat.id = :chatId")
    List<Message> findAllOfChat(long chatId);
}
