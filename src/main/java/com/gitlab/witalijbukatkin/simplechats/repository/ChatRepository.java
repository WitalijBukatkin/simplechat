/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.repository;

import com.gitlab.witalijbukatkin.simplechats.model.Chat;

import java.util.List;

public interface ChatRepository {
    Chat save(Chat chat, String userName);

    boolean delete(long id, String userName);

    Chat get(long id, String userName);

    List<Chat> getAll(String userName);

    boolean isExistUserInChat(long id, String userName);
}
