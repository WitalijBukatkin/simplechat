/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.repository.datajpa;

import com.gitlab.witalijbukatkin.simplechats.model.Chat;
import com.gitlab.witalijbukatkin.simplechats.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class DataJpaChatRepositoryImpl implements ChatRepository {

    private final CrudChatRepository repository;

    @Autowired
    public DataJpaChatRepositoryImpl(CrudChatRepository repository) {
        this.repository = repository;
    }

    @Override
    public Chat save(Chat chat, String userName) {
        if (!chat.isNew() && (get(chat.getId(), userName) == null ||
                !isExistUserInChat(chat.getId(), userName))) {
            return null;
        }
        return repository.save(chat);
    }

    @Override
    public boolean delete(long id, String userName) {
        Chat chat = get(id, userName);

        if (chat == null || !chat.getAdmin().equals(userName)) {
            return false;
        }

        repository.delete(chat);
        return true;
    }

    @Override
    public Chat get(long id, String userName) {
        Optional<Chat> chat = repository.findById(id);

        if (chat.isEmpty() || !isExistUserInChat(chat.get().getId(), userName)) {
            return null;
        }

        return chat.get();
    }

    @Override
    public List<Chat> getAll(String userName) {
        return repository.findAll(userName);
    }

    @Override
    public boolean isExistUserInChat(long id, String userName) {
        return repository.isExistUserInChat(id, userName) != null;
    }
}
