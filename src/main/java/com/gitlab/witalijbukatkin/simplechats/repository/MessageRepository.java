/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

package com.gitlab.witalijbukatkin.simplechats.repository;

import com.gitlab.witalijbukatkin.simplechats.model.Message;

import java.util.List;

public interface MessageRepository {
    Message save(Message message, long chatId, String userName);

    List<Message> getAllOfChat(long chatId, String userName);
}
