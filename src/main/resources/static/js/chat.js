/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

let chatList = $('#chats');
let chatAdmin;

function chats() {
    chatList.append("<h5>Loading...</h5>");

    if (isMobile) {
        showMenu();
    }

    if (currentChatId != null) {
        $('#chatTitle').text('');
        messageList.empty();
        currentChatId = null;
        disconnect();
    }

    $.ajax({
        type: 'get',
        url: '/ajax/chats',
        dataType: 'json',
        data: {
            userName: userName
        },
        success: function (chats) {
            chatList.empty();
            $.each(chats, function (index, chat) {
                chatList.append(`<a onclick="messages($(this), ${chat.id})" class="list-group-item">\n` +
                    `                    <div class="d-flex justify-content-between">\n` +
                    `                        <h5 class='mb-1'>${chat.name}</h5>\n` +
                    `                    </div>\n` +
                    `            </a>`);
            });

            if (chats.length === 0) {
                messageList.append("<h5>Create chat, or try refresh page</h5>");
            } else {
                chatList.find(">").first().click();
            }
        },
        error: function (xhr, status, error) {
            failNoty(xhr.responseJSON.message);
        }
    });
}

function showChatDetails() {
    chatDetails.modal('show');

    let userList = $('#userList');

    userList.append("<h5>Loading...</h5>");

    $.get('/ajax/chats/' + currentChatId, {userName: userName}, function (chat) {
        userList.empty();

        chatDetails.find('#chatNameData').val(chat.name);
        chatDetails.find('#userNameData').val('');

        chatAdmin = chat.admin;

        $.each(chat.users, function (index, user) {
            let isAdmin = chatAdmin === user;

            userList.append("<a class='list-group-item' onclick= " + (isAdmin && user !== userName ? "" : "\"showUnbindUserDialog('" + user + "')\"") + ">\n" +
                "                        <div class='d-flex justify-content-between'>\n" +
                "                            <h5 class='mb-1'>" + user + "</h5>\n" +
                (isAdmin ? "<small>admin</small>\n" : "") +
                "                        </div>\n" +
                "                    </a>");
        });
    }).fail(function (message) {
        failNoty(message);
    })
}

function createChat() {
    let chatName = $('#createChatForm > input');

    let name = chatName.val();

    if (name === null || name === '') {
        return;
    }

    $.ajax({
        type: 'post',
        url: '/ajax/chats',
        dataType: 'json',
        data: {
            userName: userName,
            name: name
        },
        success: async function () {
            chatName.val("");

            //sleep 3s
            await new Promise(resolve => setTimeout(resolve, 2000));

            chats();
        },
        error: function (xhr, status, error) {
            failNoty(xhr.responseJSON.message);
        }
    });
}

function renameChat() {
    chatDetails.modal('hide');

    let data = $('#chatNameData').val();

    if (data !== null && data.replace(' ', '') !== "") {
        $.ajax({
            type: 'put',
            url: `/ajax/chats/${currentChatId}/rename`,
            data: {
                userName: userName,
                name: data
            },
            success: function () {
                chats();
            },
            error: function (xhr, status, error) {
                failNoty(xhr.responseJSON.message);
            }
        })
    }
}

function deleteChat() {
    leaveDialog.modal('hide');

    $.ajax({
        type: 'delete',
        url: '/ajax/chats/' + currentChatId,
        data: {userName: userName},
        success: function () {
            chats();
        },
        error: function (xhr, status, error) {
            failNoty(xhr.responseJSON.message);
        }
    })
}

function unbindUser(userNameForDelete) {
    leaveDialog.modal('hide');

    $.ajax({
        type: 'put',
        url: `/ajax/chats/${currentChatId}/unbindUser`,
        data: {
            userName: userName,
            unbindUserName: userNameForDelete
        },
        success: function () {
            if (userNameForDelete === userName) {
                chats();
            }
            successNoty('user is unbind')
        },
        error: function (xhr, status, error) {
            failNoty(xhr.responseJSON.message);
        }
    })
}

function bindUser() {
    chatDetails.modal('hide');

    let data = $('#userNameData').val();

    if (data !== null && data.replace(' ', '') !== "") {
        $.ajax({
            type: 'put',
            url: `/ajax/chats/${currentChatId}/bindUser`,
            data: {
                userName: userName,
                newUserName: data
            },
            success: function () {
                successNoty('user is bind')
            },
            error: function (xhr, status, error) {
                failNoty(xhr.responseJSON.message);
            }
        })
    }
}