/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

let currentChatId = null;

let messageList = $("#messages");

function messages(chat, chatId) {
    messageList.append("<h5>Loading...</h5>");

    if (currentChatId != null) {
        chatList.find("a").removeClass("active");
        disconnect();
    }

    currentChatId = chatId;

    chat.addClass("active");

    if (isMobile) {
        hideMenu();
    }

    $('#chatTitle').text(chat.find("> > h5").text());

    $.ajax({
        type: 'get',
        url: '/ajax/chats/' + currentChatId + '/messages',
        data: {
            userName: userName
        },
        dataType: 'json',
        success: function (messages) {
            messageList.empty();

            $.each(messages, function (index, message) {
                displayMessage(message.data, message.senderId);
            });
        },
        error: function (xhr, status, error) {
            failNoty(xhr.responseJSON.message);
        }
    });

    connect();
}

function newMessage() {
    let messageData = $("#createMessageForm > input");
    let data = messageData.val();

    if (data !== null && data.replace(' ', '') !== "") {
        messageData.val("");
        sendMessage(data);
    }
}

function displayMessage(text, senderName) {
    let side, sender;

    if (senderName === userName) {
        side = "Right";
        sender = "I";
    } else {
        side = "Left";
        sender = senderName;
    }

    messageList.append(`<div class="message message${side}">\n` +
        `                   <div class="senderName"><small>${sender} <i class="fa fa-user fa-sm"></i></small></div>\n` +
        `                   <div><p>${text}</p></div><br/>\n` +
        `               </div>`);
}