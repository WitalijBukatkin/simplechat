/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

let chatDetails = $('#chatDetails');
let leaveDialog = $('#leaveDialog');
let applyLeaveDialog = leaveDialog.find('#applyLeaveDialog');

function showUnbindUserDialog(userNameForDelete) {
    chatDetails.modal('hide');

    if (userNameForDelete === userName) {
        showLeaveChatDialog();
        return;
    }

    leaveDialog.modal('show')
        .find('p').text("Do you want unbind '" + userNameForDelete + "' of chat?");

    applyLeaveDialog
        .text("Unbind")
        .attr("onclick", "unbindUser('" + userNameForDelete + "')");
}

function showLeaveChatDialog() {
    chatDetails.modal('hide');
    leaveDialog.modal('show');

    let chatName = $('#chatTitle').text();

    leaveDialog.find('p')
        .text(`Do you want leave '${chatName}' ${chatAdmin === userName ? " and delete chat?" : "?"}`);

    if (chatAdmin === userName) {
        applyLeaveDialog
            .text("Leave and delete")
            .attr("onclick", "deleteChat()");
    } else {
        applyLeaveDialog
            .attr("onclick", "unbindUser('" + userName + "')");
    }
}