/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

let isMobile = false;

function showMenu() {
    $('#containerLeft').css("display", "block");
    $('#containerRight').css("display", "none");
    isMobile = true;
}

function hideMenu() {
    $('#containerLeft').css("display", "none");
    $('#containerRight').css("display", "block");
}