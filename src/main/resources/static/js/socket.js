/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

'use strict';
let stompClient;

function connect() {
    let socket = new SockJS('/websocket');

    stompClient = Stomp.over(socket);
    stompClient.connect({}, function () {
        stompClient.subscribe('/topic/' + currentChatId, onMessageReceived);
    });
}

function disconnect() {
    stompClient.disconnect();
}

function sendMessage(text) {
    if (stompClient) {
        let message = {
            senderId: userName,
            data: text,
            type: 'TEXT'
        };

        stompClient.send("/app/" + currentChatId, {}, JSON.stringify(message));
    }
}

function onMessageReceived(payload) {
    let message = JSON.parse(payload.body);

    displayMessage(message.data, message.senderId);
}