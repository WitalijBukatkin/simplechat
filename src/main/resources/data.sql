/*
 * Copyright (c) 2020. Witalij Bukatkin
 * Github profile: https://github.com/witalijbukatkin
 */

ALTER SEQUENCE hibernate_sequence RESTART 10000;
CREATE UNIQUE INDEX ON chat_users (chat_id, users);

INSERT INTO chat (id, admin, name)
VALUES (0, 'user1', 'user1, user2'),
       (1, 'user1', 'user1, user3');

INSERT INTO chat_users (chat_id, users)
VALUES (0, 'user1'),
       (0, 'user2'),
       (1, 'user1'),
       (1, 'user3');

INSERT INTO message (id, data, sender_id, chat_id)
VALUES (0, 'Hello!!', 'user1', 0),
       (1, 'Hey', 'user3', 1);